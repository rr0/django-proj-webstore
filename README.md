# django-proj-webstore
Back-end web store template using Django and Django REST framework facilitating simple content management for categories, items and site assets. Basic features include product descriptions, images, slugs and search.

## Example of local instance
![home_demo](https://user-images.githubusercontent.com/5626537/118380410-545c7780-b596-11eb-9cf5-7eecdc0df416.png)
