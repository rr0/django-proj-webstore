from django.apps import AppConfig


class SiteassetsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'siteassets'
