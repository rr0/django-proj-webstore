from rest_framework import serializers

from .models import General

class GeneralSerializer(serializers.ModelSerializer):
    class Meta:
        model = General
        fields = (
            "id",
            "name",
            "get_absolute_url",
            "description",
            "get_image"
        )