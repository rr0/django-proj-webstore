from django.urls import path, include
from siteassets import views

urlpatterns = [
    path('siteassets/', views.SiteAssetsList.as_view()),
    path('siteassets/<slug:asset_slug>', views.SiteAsset.as_view()),
]

