from django.db.models import Q
from django.http import Http404

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.decorators import api_view

from .models import General
from .serializers import GeneralSerializer

class SiteAssetsList(APIView):
    def get(self, request, format=None):
        general_assets = General.objects.all()[:]
        serializer = GeneralSerializer(general_assets, many=True)
        return Response(serializer.data)

class SiteAsset(APIView):
    def get_object(self, asset_slug):
        try:
            return General.objects.get(slug=asset_slug)
        except General.DoesNotExist:
            raise Http404

    def get(self, request, asset_slug, format=None):
        general_asset = self.get_object(asset_slug)
        serializer = GeneralSerializer(general_asset)
        return Response(serializer.data)
